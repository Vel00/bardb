CREATE DATABASE  IF NOT EXISTS `bar_db` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `bar_db`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: bar_db
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alcohol_batches`
--

DROP TABLE IF EXISTS `alcohol_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alcohol_batches` (
  `number` int unsigned NOT NULL AUTO_INCREMENT,
  `production_date` date NOT NULL,
  `bottle_count` int NOT NULL,
  `bottle_price` int NOT NULL,
  `alcohol_brand` int unsigned NOT NULL,
  `bottles_left` int unsigned NOT NULL,
  PRIMARY KEY (`number`),
  UNIQUE KEY `number_UNIQUE` (`number`),
  KEY `alcohol_brand_idx` (`alcohol_brand`),
  CONSTRAINT `alcohol_brand_0` FOREIGN KEY (`alcohol_brand`) REFERENCES `alcohol_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alcohol_batches`
--

LOCK TABLES `alcohol_batches` WRITE;
/*!40000 ALTER TABLE `alcohol_batches` DISABLE KEYS */;
INSERT INTO `alcohol_batches` VALUES (1,'2020-03-15',16,1157,2,5),(2,'2019-06-18',20,685,5,7),(3,'2019-08-09',15,1050,3,4),(4,'2018-01-28',18,1100,4,4),(5,'2017-04-10',15,1456,1,6),(6,'2019-10-23',16,1114,2,3),(7,'2018-05-11',15,1988,7,2),(8,'2018-11-04',20,1358,9,5),(9,'2019-02-13',20,1874,6,4),(10,'2019-06-18',25,685,5,6),(11,'2019-03-26',20,929,11,5),(12,'2020-04-16',20,1429,12,3),(13,'2019-06-01',24,1399,13,4),(14,'2020-05-22',30,289,14,5),(15,'2018-12-11',12,1239,15,2),(16,'2019-08-09',15,1150,16,3),(17,'2018-12-19',20,1769,10,3),(18,'2020-01-29',25,699,18,4),(19,'2020-07-03',20,319,14,4),(20,'2019-07-30',20,1029,11,3);
/*!40000 ALTER TABLE `alcohol_batches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alcohol_batches_to_receipts`
--

DROP TABLE IF EXISTS `alcohol_batches_to_receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alcohol_batches_to_receipts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `batch_number` int unsigned NOT NULL,
  `receipt_number` int unsigned NOT NULL,
  `amount` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `batch_number_idx` (`batch_number`),
  KEY `receipt_number_idx` (`receipt_number`),
  CONSTRAINT `batch_number_1` FOREIGN KEY (`batch_number`) REFERENCES `alcohol_batches` (`number`),
  CONSTRAINT `receipt_number` FOREIGN KEY (`receipt_number`) REFERENCES `receipts` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alcohol_batches_to_receipts`
--

LOCK TABLES `alcohol_batches_to_receipts` WRITE;
/*!40000 ALTER TABLE `alcohol_batches_to_receipts` DISABLE KEYS */;
INSERT INTO `alcohol_batches_to_receipts` VALUES (1,2,7,1),(2,6,9,1),(3,7,10,1),(4,10,5,2),(5,13,17,1),(6,14,14,1),(7,18,20,1);
/*!40000 ALTER TABLE `alcohol_batches_to_receipts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alcohol_batches_to_storages`
--

DROP TABLE IF EXISTS `alcohol_batches_to_storages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alcohol_batches_to_storages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `batch_number` int unsigned NOT NULL,
  `storage_number` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `batch_number_idx` (`batch_number`),
  KEY `storage_number_idx` (`storage_number`),
  CONSTRAINT `batch_number_0` FOREIGN KEY (`batch_number`) REFERENCES `alcohol_batches` (`number`),
  CONSTRAINT `storage_number_0` FOREIGN KEY (`storage_number`) REFERENCES `storages` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alcohol_batches_to_storages`
--

LOCK TABLES `alcohol_batches_to_storages` WRITE;
/*!40000 ALTER TABLE `alcohol_batches_to_storages` DISABLE KEYS */;
INSERT INTO `alcohol_batches_to_storages` VALUES (1,2,101),(2,2,501),(3,3,102),(4,3,502),(5,4,102),(6,4,502),(7,7,101),(8,7,501),(9,8,102),(10,8,401),(11,8,502),(12,10,111),(13,10,511),(14,11,112),(15,11,412),(16,12,111),(17,12,512),(18,13,212),(19,13,511),(20,15,111),(21,15,211),(22,19,101),(23,19,401),(24,20,102),(25,20,502);
/*!40000 ALTER TABLE `alcohol_batches_to_storages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alcohol_brands`
--

DROP TABLE IF EXISTS `alcohol_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alcohol_brands` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `volume` float NOT NULL,
  `strength` int NOT NULL,
  `alcohol_class` int unsigned NOT NULL,
  `manufacturer` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `alcohol_class_idx` (`alcohol_class`),
  KEY `manufacturer_idx` (`manufacturer`),
  KEY `brand_name` (`name`),
  CONSTRAINT `alcohol_class` FOREIGN KEY (`alcohol_class`) REFERENCES `alcohol_classes` (`id`),
  CONSTRAINT `manufacturer` FOREIGN KEY (`manufacturer`) REFERENCES `manufacturers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alcohol_brands`
--

LOCK TABLES `alcohol_brands` WRITE;
/*!40000 ALTER TABLE `alcohol_brands` DISABLE KEYS */;
INSERT INTO `alcohol_brands` VALUES (1,'Havana club(светлый)',1000,40,5,1),(2,'Olmeca(silver)',1000,38,8,2),(3,'Bols triple sec',700,21,7,3),(4,'Bols coffee',700,21,7,3),(5,'Finlandia',700,40,0,4),(6,'Havana club(темный)',1000,40,5,1),(7,'Jameson',1000,40,4,5),(8,'Irish Cream',700,15,7,6),(9,'Beefeater',750,47,9,7),(10,'Gordons',1000,40,9,16),(11,'Rosso MARTINI',1000,15,11,17),(12,'CAMPARI Bitter',1000,25,12,18),(13,'Jim Beam',1000,40,13,19),(14,'Tundra',500,40,0,20),(15,'ANGOSTURA Bitter',200,44,12,21),(16,'Bols cherry',700,21,7,3),(17,'Bols peppermint',700,21,7,3),(18,'Prosecco',750,11,10,22);
/*!40000 ALTER TABLE `alcohol_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alcohol_brands_to_types`
--

DROP TABLE IF EXISTS `alcohol_brands_to_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alcohol_brands_to_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `alcohol_brand_id` int unsigned NOT NULL,
  `alcohol_type_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `alcohol_brand_idx` (`alcohol_brand_id`),
  KEY `alcohol_type_idx` (`alcohol_type_id`),
  CONSTRAINT `alcohol_brand` FOREIGN KEY (`alcohol_brand_id`) REFERENCES `alcohol_brands` (`id`),
  CONSTRAINT `alcohol_type` FOREIGN KEY (`alcohol_type_id`) REFERENCES `alcohol_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alcohol_brands_to_types`
--

LOCK TABLES `alcohol_brands_to_types` WRITE;
/*!40000 ALTER TABLE `alcohol_brands_to_types` DISABLE KEYS */;
INSERT INTO `alcohol_brands_to_types` VALUES (1,1,16),(2,2,26),(3,3,30),(4,4,31),(5,6,18),(6,7,14),(7,8,34),(8,9,27),(9,11,39);
/*!40000 ALTER TABLE `alcohol_brands_to_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alcohol_classes`
--

DROP TABLE IF EXISTS `alcohol_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alcohol_classes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alcohol_classes`
--

LOCK TABLES `alcohol_classes` WRITE;
/*!40000 ALTER TABLE `alcohol_classes` DISABLE KEYS */;
INSERT INTO `alcohol_classes` VALUES (0,'Водка'),(1,'Пиво'),(2,'Вино'),(3,'Коньяк'),(4,'Виски'),(5,'Ром'),(6,'Абсент'),(7,'Ликер'),(8,'Текила'),(9,'Джин'),(10,'Шампанское'),(11,'Вермут'),(12,'Биттер'),(13,'Бурбон');
/*!40000 ALTER TABLE `alcohol_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alcohol_types`
--

DROP TABLE IF EXISTS `alcohol_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alcohol_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alcohol_class` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `alcohol_class_idx` (`alcohol_class`),
  CONSTRAINT `alcohol_class_0` FOREIGN KEY (`alcohol_class`) REFERENCES `alcohol_classes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alcohol_types`
--

LOCK TABLES `alcohol_types` WRITE;
/*!40000 ALTER TABLE `alcohol_types` DISABLE KEYS */;
INSERT INTO `alcohol_types` VALUES (1,'Светлое',1),(2,'Темное',1),(3,'Фильтрованное',1),(4,'Нефильрованное',1),(5,'Белое',2),(6,'Красное',2),(7,'Розовое',2),(8,'Сладкое',2),(9,'Полусладкое',2),(10,'Полусухое',2),(11,'Сухое',2),(12,'Крепленое',2),(13,'Односолодовый ',4),(14,'Купажированный ',4),(15,'Зерновой ',4),(16,'Светлый',5),(17,'Золотой',5),(18,'Темный',5),(19,'Синий',6),(20,'Желтый',6),(21,'Красный',6),(22,'Голубой',6),(23,'Белый',6),(24,'Черный',6),(25,'Премиум(gold)',8),(26,'Стандартная(silver)',8),(27,'Английский',9),(28,'Голандский',9),(29,'Игристое',2),(30,'Фруктовый',7),(31,'Кофейный',7),(32,'Травяной или пряный',7),(33,'Мятный',7),(34,'Сливочный',7),(35,'Ягодный',7),(36,'Ореховый',7),(37,'Сухой',11),(38,'Белый',11),(39,'Красный',11),(40,'Розовый',11),(41,'Горький',11);
/*!40000 ALTER TABLE `alcohol_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `barmens`
--

DROP TABLE IF EXISTS `barmens`;
/*!50001 DROP VIEW IF EXISTS `barmens`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `barmens` AS SELECT 
 1 AS `contract_number`,
 1 AS `date_of_birth`,
 1 AS `name`,
 1 AS `wage`,
 1 AS `sex`,
 1 AS `position`,
 1 AS `bar_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `bars`
--

DROP TABLE IF EXISTS `bars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bars` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `adress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bars`
--

LOCK TABLES `bars` WRITE;
/*!40000 ALTER TABLE `bars` DISABLE KEYS */;
INSERT INTO `bars` VALUES (1,'Улица Советская 17'),(2,'Улица Комсомольская 4');
/*!40000 ALTER TABLE `bars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cocktails`
--

DROP TABLE IF EXISTS `cocktails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cocktails` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cocktails`
--

LOCK TABLES `cocktails` WRITE;
/*!40000 ALTER TABLE `cocktails` DISABLE KEYS */;
INSERT INTO `cocktails` VALUES (1,'Мохито',350),(2,'Маргарита',400),(3,'Белый русский',350),(4,'Б-52',300),(5,'Дайкири',400),(6,'Пина колада',400),(7,'Куба либре',300),(8,'Отвертка',300),(9,'Том коллинз',350),(10,'Чёрный русский',300),(11,'Ирландский кофе',450),(12,'Негрони',350),(13,'Лонг айленд айс ти',500),(14,'Манхэттен',350),(15,'Олд фешен',400),(16,'Бульвардье',350),(17,'Американо',300),(18,'Белая леди',300),(19,'Кузнечик',350),(20,'Роб рой',350);
/*!40000 ALTER TABLE `cocktails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cocktails_to_alcohol_brands`
--

DROP TABLE IF EXISTS `cocktails_to_alcohol_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cocktails_to_alcohol_brands` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `brand_id` int unsigned NOT NULL,
  `cocktail_id` int unsigned NOT NULL,
  `amount` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `brand_id_idx` (`brand_id`),
  KEY `cocktail_id_idx` (`cocktail_id`),
  CONSTRAINT `brand_id` FOREIGN KEY (`brand_id`) REFERENCES `alcohol_brands` (`id`),
  CONSTRAINT `cocktail_id_2` FOREIGN KEY (`cocktail_id`) REFERENCES `cocktails` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cocktails_to_alcohol_brands`
--

LOCK TABLES `cocktails_to_alcohol_brands` WRITE;
/*!40000 ALTER TABLE `cocktails_to_alcohol_brands` DISABLE KEYS */;
INSERT INTO `cocktails_to_alcohol_brands` VALUES (1,1,1,50),(2,2,2,50),(3,3,2,25),(4,4,3,30),(5,5,3,30),(6,3,4,15),(7,4,4,15),(8,8,4,15),(9,1,5,60),(10,1,6,60),(11,6,6,15),(12,6,7,50),(13,5,8,50),(14,9,9,50),(15,4,10,25),(16,5,10,50),(17,7,11,45),(18,10,12,30),(19,11,12,30),(20,12,12,30),(21,10,13,30),(22,5,13,30),(23,1,13,30),(24,2,13,30),(25,3,13,30),(26,13,14,50),(27,11,14,25),(28,15,14,1),(29,13,15,50),(30,15,15,1),(31,13,16,45),(32,11,16,30),(33,12,16,30);
/*!40000 ALTER TABLE `cocktails_to_alcohol_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cocktails_to_glasses`
--

DROP TABLE IF EXISTS `cocktails_to_glasses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cocktails_to_glasses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cocktail_id` int unsigned NOT NULL,
  `glass_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `glass_id_idx` (`glass_id`),
  KEY `cocktail_id_idx` (`cocktail_id`),
  CONSTRAINT `cocktail_id` FOREIGN KEY (`cocktail_id`) REFERENCES `cocktails` (`id`),
  CONSTRAINT `glass_id` FOREIGN KEY (`glass_id`) REFERENCES `glasses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cocktails_to_glasses`
--

LOCK TABLES `cocktails_to_glasses` WRITE;
/*!40000 ALTER TABLE `cocktails_to_glasses` DISABLE KEYS */;
INSERT INTO `cocktails_to_glasses` VALUES (1,1,2),(2,2,9),(3,3,4),(4,4,10),(5,5,8),(6,6,1),(7,7,3),(8,8,3),(9,9,3),(10,10,4),(11,11,11),(14,12,4),(15,13,2),(16,14,9),(17,15,4),(18,16,4),(19,17,4),(20,18,8),(21,19,9),(22,20,9);
/*!40000 ALTER TABLE `cocktails_to_glasses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cocktails_to_ingredients`
--

DROP TABLE IF EXISTS `cocktails_to_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cocktails_to_ingredients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `ingredient_id` int unsigned NOT NULL,
  `cocktail_id` int unsigned NOT NULL,
  `amount` int NOT NULL,
  `units` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `ingredient_id_idx` (`ingredient_id`),
  KEY `cocktail_id_idx` (`cocktail_id`),
  CONSTRAINT `cocktail_id_1` FOREIGN KEY (`cocktail_id`) REFERENCES `cocktails` (`id`),
  CONSTRAINT `ingredient_id` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cocktails_to_ingredients`
--

LOCK TABLES `cocktails_to_ingredients` WRITE;
/*!40000 ALTER TABLE `cocktails_to_ingredients` DISABLE KEYS */;
INSERT INTO `cocktails_to_ingredients` VALUES (1,10,1,15,'мл'),(2,7,1,100,'мл'),(3,6,1,80,'г'),(4,5,1,3,'г'),(5,16,1,200,'г'),(6,10,2,10,'мл'),(7,6,2,30,'мл'),(8,8,2,10,'г'),(9,11,2,2,'г'),(10,2,2,200,'г'),(11,14,3,30,'мл'),(12,2,3,120,'г'),(13,10,5,15,'мл'),(14,8,5,30,'мл'),(15,2,5,200,'г'),(16,17,6,40,'мл'),(17,8,6,20,'мл'),(18,30,6,150,'г'),(19,16,6,60,'г'),(20,8,7,10,'мл'),(21,9,7,140,'мл'),(22,6,7,20,'г'),(23,2,7,180,'г'),(24,18,8,150,'мл'),(25,3,8,30,'г'),(26,2,8,180,'г'),(27,10,9,25,'мл'),(28,19,9,25,'мл'),(29,7,9,100,'мл'),(30,3,9,30,'г'),(31,4,9,5,'г'),(32,2,9,380,'г');
/*!40000 ALTER TABLE `cocktails_to_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cocktails_to_receipts`
--

DROP TABLE IF EXISTS `cocktails_to_receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cocktails_to_receipts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `receipt_number` int unsigned NOT NULL,
  `cocktail_id` int unsigned NOT NULL,
  `amount` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `cocktail_id_idx` (`cocktail_id`),
  KEY `receipt_number_idx` (`receipt_number`),
  CONSTRAINT `cocktail_id_0` FOREIGN KEY (`cocktail_id`) REFERENCES `cocktails` (`id`),
  CONSTRAINT `receipt_number_0` FOREIGN KEY (`receipt_number`) REFERENCES `receipts` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cocktails_to_receipts`
--

LOCK TABLES `cocktails_to_receipts` WRITE;
/*!40000 ALTER TABLE `cocktails_to_receipts` DISABLE KEYS */;
INSERT INTO `cocktails_to_receipts` VALUES (1,1,2,1),(2,1,5,1),(3,3,3,1),(4,3,10,1),(5,4,9,2),(6,4,11,1),(7,8,4,5),(8,9,5,1),(9,8,6,1),(10,8,8,3),(11,10,7,1),(12,10,15,2),(13,12,12,1),(14,13,14,2),(15,13,3,2),(16,13,19,1),(17,16,3,1),(18,16,6,2),(19,17,12,1),(20,17,7,1),(21,18,17,3),(22,19,5,2),(23,19,8,1),(24,19,9,4),(25,20,16,2),(26,20,1,1);
/*!40000 ALTER TABLE `cocktails_to_receipts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `coming_shifts`
--

DROP TABLE IF EXISTS `coming_shifts`;
/*!50001 DROP VIEW IF EXISTS `coming_shifts`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `coming_shifts` AS SELECT 
 1 AS `name`,
 1 AS `date`,
 1 AS `time_from`,
 1 AS `time_to`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employees` (
  `contract_number` int unsigned NOT NULL,
  `date_of_birth` date NOT NULL,
  `name` varchar(255) NOT NULL,
  `wage` int NOT NULL,
  `sex` tinyint NOT NULL,
  `position` int unsigned NOT NULL,
  `bar_id` int unsigned NOT NULL,
  PRIMARY KEY (`contract_number`),
  UNIQUE KEY `contract_number_UNIQUE` (`contract_number`),
  KEY `position_id_idx` (`position`),
  KEY `bar_id_0_idx` (`bar_id`),
  KEY `employee_name` (`name`),
  CONSTRAINT `bar_id_0` FOREIGN KEY (`bar_id`) REFERENCES `bars` (`id`),
  CONSTRAINT `position_id` FOREIGN KEY (`position`) REFERENCES `positions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (111111,'1989-05-12','Анатолий',33000,1,1,1),(222222,'1991-09-21','Максим',27000,1,1,1),(333333,'1991-06-13','Сергей',20000,1,3,1),(444444,'1990-08-31','Алиса',40000,0,2,1),(555555,'1990-07-24','Дмитрий',45000,1,2,2),(666666,'1991-12-14','Николай',33000,1,1,2),(777777,'1989-06-18','Денис',23000,1,3,2),(888888,'1995-07-23','Андрей',27000,1,1,2),(999999,'1993-06-14','Анна',33000,0,1,2);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glasses`
--

DROP TABLE IF EXISTS `glasses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `glasses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `volume` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glasses`
--

LOCK TABLES `glasses` WRITE;
/*!40000 ALTER TABLE `glasses` DISABLE KEYS */;
INSERT INTO `glasses` VALUES (1,'Харрикейн',400),(2,'Хайболл',350),(3,'Коллинз',300),(4,'Олд-фэшен',200),(5,'Шот',50),(6,'Бокал для пива',500),(7,'Бокал для коньяка',150),(8,'Бокал для мартини',90),(9,'Бокал для Маргариты',250),(10,'Рюмка для ликера',60),(11,'Бокал для ирландского кофе',250);
/*!40000 ALTER TABLE `glasses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient_batches`
--

DROP TABLE IF EXISTS `ingredient_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ingredient_batches` (
  `number` int unsigned NOT NULL AUTO_INCREMENT,
  `production_date` date NOT NULL,
  `expiration_date` date NOT NULL,
  `count_remaining` int NOT NULL,
  `manufacturer` int unsigned NOT NULL,
  `ingredient` int unsigned NOT NULL,
  PRIMARY KEY (`number`),
  UNIQUE KEY `number_UNIQUE` (`number`),
  KEY `ingredient_id_idx` (`ingredient`),
  KEY `manufacturer_idx` (`manufacturer`),
  KEY `ingredient_production_date` (`production_date`),
  CONSTRAINT `ingredient_id_0` FOREIGN KEY (`ingredient`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `manufacturer_0` FOREIGN KEY (`manufacturer`) REFERENCES `manufacturers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient_batches`
--

LOCK TABLES `ingredient_batches` WRITE;
/*!40000 ALTER TABLE `ingredient_batches` DISABLE KEYS */;
INSERT INTO `ingredient_batches` VALUES (1,'2020-12-19','2022-12-19',20,11,10),(2,'2020-02-16','2021-02-16',18,15,7),(3,'2020-08-28','2020-08-28',40,8,6),(4,'2020-09-28','2020-10-04',15,9,5),(5,'2020-08-08','2021-08-08',16,13,8),(6,'2020-09-02','2022-09-02',5,12,11),(7,'2020-09-05','2020-10-05',3,10,14),(8,'2019-10-12','2021-10-12',12,11,17),(9,'2020-08-25','2020-11-25',30,8,3),(10,'2020-04-03','2021-04-03',6,13,19),(11,'2020-06-20','2021-06-20',10,15,7),(12,'2020-08-17','2020-09-07',2,9,4),(13,'2020-09-18','2020-11-18',11,8,1),(14,'2020-09-09','2021-09-09',5,13,19),(15,'2020-11-08','2021-11-08',6,13,18),(16,'2019-11-21','2021-11-21',4,11,26),(17,'2020-10-10','2020-11-10',4,10,14),(18,'2020-07-13','2021-07-13',9,13,8);
/*!40000 ALTER TABLE `ingredient_batches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient_batches_to_storages`
--

DROP TABLE IF EXISTS `ingredient_batches_to_storages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ingredient_batches_to_storages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `batch_number` int unsigned NOT NULL,
  `storage_number` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `batch_number_idx` (`batch_number`),
  KEY `storage_number_idx` (`storage_number`),
  CONSTRAINT `batch_number` FOREIGN KEY (`batch_number`) REFERENCES `ingredient_batches` (`number`),
  CONSTRAINT `storage_number` FOREIGN KEY (`storage_number`) REFERENCES `storages` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient_batches_to_storages`
--

LOCK TABLES `ingredient_batches_to_storages` WRITE;
/*!40000 ALTER TABLE `ingredient_batches_to_storages` DISABLE KEYS */;
INSERT INTO `ingredient_batches_to_storages` VALUES (0,1,201),(1,1,502),(2,4,101),(3,4,401),(4,3,101),(5,3,401),(6,5,102),(7,5,501),(8,7,401),(9,7,402),(10,9,111),(11,9,211),(12,10,413),(13,10,512),(14,11,112),(15,11,411),(16,13,401),(17,13,501),(18,18,111),(19,18,211),(20,14,401),(21,14,402);
/*!40000 ALTER TABLE `ingredient_batches_to_storages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ingredients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `ingredient_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients`
--

LOCK TABLES `ingredients` WRITE;
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` VALUES (30,'Ананас'),(3,'Апельсин'),(28,'Апельсиновая цедра'),(4,'Вишня'),(20,'Вода'),(27,'Грейпфрут'),(9,'Кола'),(13,'Кофе'),(6,'Лайм'),(16,'Лед дробленый'),(2,'Лед кубиками'),(1,'Лимон'),(29,'Лимонная цедра'),(15,'Молоко'),(5,'Мята'),(12,'Сахар'),(26,'Сироп гранатовый'),(17,'Сироп кокосовый'),(10,'Сироп сахарный'),(14,'Сливки'),(7,'Содовая'),(25,'Сок ананасовый'),(18,'Сок апельсиновый'),(8,'Сок лаймовый'),(19,'Сок лимонный'),(11,'Соль');
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_to_storage_types`
--

DROP TABLE IF EXISTS `ingredients_to_storage_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ingredients_to_storage_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `ingredient_id` int unsigned NOT NULL,
  `storage_type_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `storage_type_idx` (`storage_type_id`),
  KEY `ingredient_id_idx` (`ingredient_id`),
  CONSTRAINT `ingredient_id_1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `storage_type_1` FOREIGN KEY (`storage_type_id`) REFERENCES `storage_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_to_storage_types`
--

LOCK TABLES `ingredients_to_storage_types` WRITE;
/*!40000 ALTER TABLE `ingredients_to_storage_types` DISABLE KEYS */;
INSERT INTO `ingredients_to_storage_types` VALUES (1,1,1),(2,1,4),(3,3,1),(4,3,4),(5,4,1),(6,4,4),(7,5,1),(8,5,4),(9,6,1),(10,6,4),(11,7,1),(12,7,4),(13,8,1),(14,8,4),(15,9,1),(16,9,4),(17,14,1),(18,14,4),(19,15,1),(20,15,4);
/*!40000 ALTER TABLE `ingredients_to_storage_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturers`
--

DROP TABLE IF EXISTS `manufacturers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manufacturers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturers`
--

LOCK TABLES `manufacturers` WRITE;
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
INSERT INTO `manufacturers` VALUES (1,'Хосе Аречабалы'),(2,'Pernod Ricard'),(3,'Bols Amsterdam'),(4,'Altia'),(5,'Jameson'),(6,'Rober A. Merry & Co'),(7,'Pernod Ricard'),(8,'ROYAL FRUITS'),(9,'Фрукты и овощи'),(10,'ВолгоМясоМолТорг'),(11,'Guiot Sas'),(12,'РусСоль'),(13,'LimoChef'),(14,'Святой Источник'),(15,'SodaStream'),(16,'Gordon\'s'),(17,'Martini'),(18,'Campari'),(19,'JIM BEAM'),(20,'Татспиртпром'),(21,'Angostura'),(22,'Gancia');
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `positions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idpositions_UNIQUE` (`id`),
  KEY `position_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions`
--

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` VALUES (2,'Администратор'),(1,'Бармен'),(3,'Охранник');
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receipts`
--

DROP TABLE IF EXISTS `receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `receipts` (
  `number` int unsigned NOT NULL AUTO_INCREMENT,
  `time` time NOT NULL,
  `date` date NOT NULL,
  `shift_id` int unsigned NOT NULL,
  PRIMARY KEY (`number`),
  UNIQUE KEY `number_UNIQUE` (`number`),
  KEY `shift_id_idx` (`shift_id`),
  KEY `receipt_date_time` (`date`,`time`),
  CONSTRAINT `shift_id` FOREIGN KEY (`shift_id`) REFERENCES `shifts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receipts`
--

LOCK TABLES `receipts` WRITE;
/*!40000 ALTER TABLE `receipts` DISABLE KEYS */;
INSERT INTO `receipts` VALUES (1,'19:03:00','2020-10-12',1),(2,'19:54:00','2020-10-12',1),(3,'22:08:00','2020-10-12',1),(4,'21:26:00','2020-10-15',4),(5,'23:01:00','2020-10-15',4),(6,'00:35:00','2020-10-16',4),(7,'20:46:00','2020-10-17',5),(8,'18:50:00','2020-10-17',5),(9,'22:45:00','2020-10-17',5),(10,'22:46:00','2020-10-17',5),(11,'20:12:00','2020-11-17',11),(12,'21:08:00','2020-11-17',11),(13,'22:23:00','2020-11-17',11),(14,'18:19:00','2020-11-20',16),(15,'19:35:00','2020-11-20',17),(16,'19:16:00','2020-11-20',16),(17,'21:38:00','2020-11-20',17),(18,'23:21:00','2020-11-20',17),(19,'00:42:00','2020-11-21',16),(20,'02:56:00','2020-11-21',17);
/*!40000 ALTER TABLE `receipts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shifts`
--

DROP TABLE IF EXISTS `shifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shifts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time_from` time NOT NULL,
  `time_to` time NOT NULL,
  `employee` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idshifts_UNIQUE` (`id`),
  KEY `employee_id_idx` (`employee`),
  CONSTRAINT `employee` FOREIGN KEY (`employee`) REFERENCES `employees` (`contract_number`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shifts`
--

LOCK TABLES `shifts` WRITE;
/*!40000 ALTER TABLE `shifts` DISABLE KEYS */;
INSERT INTO `shifts` VALUES (1,'2020-10-12','18:00:00','03:00:00',111111),(2,'2020-10-13','18:00:00','03:00:00',111111),(3,'2020-10-14','18:00:00','03:00:00',222222),(4,'2020-10-15','18:00:00','03:00:00',222222),(5,'2020-10-17','18:00:00','05:00:00',111111),(6,'2020-10-17','18:00:00','05:00:00',222222),(7,'2020-10-14','18:00:00','03:00:00',333333),(8,'2020-10-15','18:00:00','03:00:00',333333),(9,'2020-10-15','17:00:00','03:00:00',444444),(10,'2020-10-27','18:00:00','05:00:00',444444),(11,'2020-11-17','18:00:00','03:00:00',666666),(12,'2020-11-18','18:00:00','03:00:00',666666),(13,'2020-11-18','16:00:00','03:00:00',555555),(14,'2020-11-20','18:00:00','05:00:00',777777),(15,'2020-11-21','18:00:00','05:00:00',777777),(16,'2020-11-20','18:00:00','05:00:00',888888),(17,'2020-11-20','18:00:00','05:00:00',999999),(18,'2020-11-21','18:00:00','06:00:00',888888),(19,'2020-11-21','18:00:00','06:00:00',999999),(20,'2020-11-22','18:00:00','03:00:00',555555),(21,'2020-12-01','18:00:00','03:00:00',222222);
/*!40000 ALTER TABLE `shifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storage_types`
--

DROP TABLE IF EXISTS `storage_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storage_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storage_types`
--

LOCK TABLES `storage_types` WRITE;
/*!40000 ALTER TABLE `storage_types` DISABLE KEYS */;
INSERT INTO `storage_types` VALUES (1,'Холодильник'),(2,'Шкаф'),(3,'Морозильная камера'),(4,'Мини-холодильник'),(5,'Стеллаж');
/*!40000 ALTER TABLE `storage_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storages`
--

DROP TABLE IF EXISTS `storages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storages` (
  `bar_id` int unsigned NOT NULL,
  `number` int unsigned NOT NULL AUTO_INCREMENT,
  `storage_type` int unsigned NOT NULL,
  PRIMARY KEY (`number`),
  UNIQUE KEY `number_UNIQUE` (`number`),
  KEY `storage_type_idx` (`storage_type`),
  KEY `bar_id_idx` (`bar_id`),
  CONSTRAINT `bar_id` FOREIGN KEY (`bar_id`) REFERENCES `bars` (`id`),
  CONSTRAINT `storage_type` FOREIGN KEY (`storage_type`) REFERENCES `storage_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=513 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storages`
--

LOCK TABLES `storages` WRITE;
/*!40000 ALTER TABLE `storages` DISABLE KEYS */;
INSERT INTO `storages` VALUES (1,101,1),(1,102,1),(2,111,1),(2,112,1),(2,113,1),(1,201,2),(2,211,2),(2,212,2),(1,301,3),(1,302,3),(2,311,3),(2,312,3),(1,401,4),(1,402,4),(2,411,4),(2,412,4),(2,413,4),(1,501,5),(1,502,5),(2,511,5),(2,512,5);
/*!40000 ALTER TABLE `storages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bar_db'
--

--
-- Dumping routines for database 'bar_db'
--
/*!50003 DROP FUNCTION IF EXISTS `is_alcohol` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `is_alcohol`(alcohol_id INT UNSIGNED) RETURNS int
    READS SQL DATA
    DETERMINISTIC
BEGIN
	DECLARE c INT DEFAULT 0;
	SELECT COUNT(*) INTO c FROM alcohol_batches WHERE alcohol_brand = alcohol_id AND bottles_left != 0;
	IF c = 0 THEN
		RETURN 0;
    END IF;
RETURN 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_cocktail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `is_cocktail`(cocktail_name VARCHAR(255)) RETURNS int
    READS SQL DATA
    DETERMINISTIC
BEGIN
	DECLARE cocktail_id INT DEFAULT (SELECT id FROM cocktails WHERE name = cocktail_name);
	DECLARE done INT DEFAULT FALSE;
	DECLARE i INT DEFAULT 0;
	DECLARE cur CURSOR FOR SELECT ingredient_id FROM cocktails_to_ingredients WHERE cocktail_id = id;
    DECLARE cur1 CURSOR FOR SELECT brand_id FROM cocktails_to_alcohol_brands WHERE cocktail_id = id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur;
    i_loop: LOOP 
		FETCH cur INTO i;
        IF done THEN
			LEAVE i_loop;
        END IF;
        IF is_ingredient(i) = 0 THEN 
			RETURN 0;
		END IF;
    END LOOP;
	CLOSE cur;
	
    SET done = FALSE;
    OPEN cur1;
    i_loop1: LOOP
		FETCH cur1 INTO i;
        IF done THEN
			LEAVE i_loop1;
		END IF;
        IF is_alcohol(i) = 0 THEN
			RETURN 0;
		END IF;
    END LOOP;
	CLOSE cur1;
   
	RETURN 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_ingredient` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `is_ingredient`(ingredient_id INT UNSIGNED) RETURNS int
    READS SQL DATA
    DETERMINISTIC
BEGIN
	DECLARE c INT DEFAULT 0;
	SELECT COUNT(*) INTO c FROM ingredient_batches WHERE ingredient = ingredient_id AND count_remaining != 0;
	IF c = 0 THEN
		RETURN 0;
    END IF;
	RETURN 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `period_sum` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `period_sum`(period_start DATE, period_end DATE) RETURNS int
    READS SQL DATA
    DETERMINISTIC
BEGIN
	DECLARE total_sum INT DEFAULT 0;
    SELECT SUM(receipt_sum(number)) INTO total_sum FROM receipts WHERE  date BETWEEN period_start AND period_end;
	RETURN total_sum;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `receipt_sum` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `receipt_sum`(r_number INT UNSIGNED) RETURNS int
    READS SQL DATA
    DETERMINISTIC
BEGIN
	DECLARE total_sum INT DEFAULT 0;
	SELECT SUM(total) INTO total_sum FROM
	(
		SELECT * FROM
		(
		SELECT bottle_price * amount AS total FROM 
			(SELECT number, bottle_price FROM alcohol_batches) AS a_price
		NATURAL JOIN 
			(SELECT amount, batch_number AS number FROM alcohol_batches_to_receipts WHERE receipt_number = r_number) AS a_amount
		) AS a_total
	UNION ALL
		(
		SELECT price * amount AS total FROM 
			(SELECT id, price FROM cocktails) AS c_price
		NATURAL JOIN 
			(SELECT amount, cocktail_id AS id FROM cocktails_to_receipts WHERE receipt_number = r_number) AS c_amount
		) 
	) AS t;
RETURN total_sum;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_cocktail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_cocktail`(alcohol_name VARCHAR(255), ingredient_name VARCHAR(255))
BEGIN
	SELECT name FROM
	(SELECT cocktail_id FROM cocktails_to_alcohol_brands WHERE brand_id = (SELECT id FROM alcohol_brands WHERE name = alcohol_name)
	AND cocktail_id IN
	(SELECT cocktail_id FROM cocktails_to_ingredients WHERE ingredient_id = (SELECT id FROM ingredients WHERE name = ingredient_name))) AS t1 
	JOIN cocktails ON cocktail_id = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cocktail_stats` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cocktail_stats`()
BEGIN
	SELECT cocktail_name, brand_name, amount, strength FROM
	(SELECT cocktail_name, brand_id, amount FROM ((SELECT name AS cocktail_name, id AS c_id FROM cocktails) As t1  
	JOIN cocktails_to_alcohol_brands ON c_id = cocktail_id)) AS t2
	JOIN (SELECT name AS brand_name, strength, id AS b_id FROM alcohol_brands) AS t3 ON b_id = brand_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `count_receipts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `count_receipts`(period_start DATE, period_end DATE)
BEGIN
	DECLARE c INT DEFAULT (SELECT COUNT(*) FROM receipts WHERE date BETWEEN period_start AND period_end);
	IF c > 40 THEN
		SELECT 'Много';
	ELSEIF c < 15 THEN
		SELECT 'Мало';
	ELSE 
		SELECT 'Средне';
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `barmens`
--

/*!50001 DROP VIEW IF EXISTS `barmens`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `barmens` AS select `employees`.`contract_number` AS `contract_number`,`employees`.`date_of_birth` AS `date_of_birth`,`employees`.`name` AS `name`,`employees`.`wage` AS `wage`,`employees`.`sex` AS `sex`,`employees`.`position` AS `position`,`employees`.`bar_id` AS `bar_id` from `employees` where (`employees`.`position` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `coming_shifts`
--

/*!50001 DROP VIEW IF EXISTS `coming_shifts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `coming_shifts` AS select `employees`.`name` AS `name`,`soon_shift`.`date` AS `date`,`soon_shift`.`time_from` AS `time_from`,`soon_shift`.`time_to` AS `time_to` from ((select `shifts`.`id` AS `id`,`shifts`.`employee` AS `employee`,`shifts`.`date` AS `date`,`shifts`.`time_from` AS `time_from`,`shifts`.`time_to` AS `time_to` from `shifts` where (`shifts`.`date` between curdate() and (curdate() + interval 3 day))) `soon_shift` join `employees` on((`employees`.`contract_number` = `soon_shift`.`employee`))) order by `employees`.`name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-01 11:57:46
