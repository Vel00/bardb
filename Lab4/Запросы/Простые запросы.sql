-- Запросы на: SELECT, DISTINCT, WHERE, AND/OR/NOT, IN, BETWEEN, IS NULL, AS
#1
SELECT name FROM cocktails
WHERE price = 300 OR price = 400;

#2
SELECT DISTINCT price FROM cocktails ORDER BY price;

#3
SELECT volume FROM glasses  WHERE NOT (id < 5 AND volume > 250);

#4
SELECT name FROM alcohol_brands WHERE strength IN(38, 40, 47);
	
#5
SELECT name FROM employees WHERE date_of_birth BETWEEN '1990-08-31' AND '1993-06-14';

#6
SELECT name FROM alcohol_brands WHERE volume = 750 OR volume = 1000;

#7
SELECT number FROM alcohol_batches WHERE production_date NOT IN('2020-03-15', '2019-06-18', '2019-06-18');

#8
SELECT id FROM alcohol_batches_to_receipts WHERE batch_number = 2 OR batch_number = 10;

#9 
SELECT ingredient AS ingredien_number FROM ingredient_batches WHERE expiration_date BETWEEN '2020-10-28' AND '2021-04-03';

#10
SELECT number FROM ingredient_batches WHERE count_remaining = 18 OR count_remaining = 30;

#11
SELECT name FROM storage_types WHERE id IN(1, 2, 3);

#12
SELECT name FROM employees  WHERE sex = 1 AND wage > 30000;

#13
SELECT name FROM alcohol_types WHERE alcohol_class = 5 OR alcohol_class = 7;

#14 
SELECT DISTINCT bottles_left FROM alcohol_batches;

#15
SELECT name FROM glasses WHERE volume NOT BETWEEN 100 AND 300;

#
SELECT storage_number FROM alcohol_batches_to_storages WHERE batch_number > 2 AND batch_number !=7;

#
SELECT date_of_birth, wage FROM employees WHERE name IN('Алиса', 'Дмитрий');

#
SELECT adress AS street_and_number FROM bars WHERE id < 2;

#
SELECT name FROM cocktails WHERE price BETWEEN 350 AND 400;

#
SELECT ingredient_id, units FROM cocktails_to_ingredients WHERE amount NOT IN('10', '100', '200') ORDER BY ingredient_id;

#
SELECT DISTINCT glass_id FROM cocktails_to_glasses;

#
SELECT storage_number FROM ingredient_batches_to_storages WHERE NOT batch_number = 4; 

#
SELECT name, id FROM manufacturers WHERE NOT id = 2 AND id < 14 ORDER BY id;

#
SELECT DISTINCT storage_type FROM storages WHERE number IN('101', '201', '301', '401', '501');

#
SELECT date_of_birth As birthday, name FROM employees WHERE contract_number < 666666 AND wage > 30000 OR wage = 27000;

-- Запросы на: LIKE
#16 
SELECT name FROM employees WHERE name LIKE 'а%';

#17
SELECT name FROM glasses WHERE name LIKE '%б%';

#18
SELECT name FROM manufacturers WHERE name LIKE '%С%' AND name NOT LIKE 'С%';

#19
SELECT name FROM ingredients WHERE name LIKE '%и%' ORDER BY name;

#20 
SELECT name AS `Наименование` FROM alcohol_types WHERE name LIKE '%ое' ORDER BY name;

-- Запросы на: COUNT, MAX, MIN, SUM, AVG, GROUP BY, HAVING 
#21
SELECT COUNT(*) FROM manufacturers WHERE name LIKE '%а%' OR name LIKE '%о%' OR name LIKE '%и%' OR name LIKE '%е%';

#22
SELECT MAX(wage) FROM employees;

#23
SELECT MIN(strength) FROM alcohol_brands;

#24
SELECT SUM(bottles_left) FROM alcohol_batches;

#25
SELECT AVG(price) FROM cocktails;

#26
SELECT MAX(strength), alcohol_class FROM alcohol_brands GROUP BY alcohol_class; 

#27
SELECT AVG(wage) FROM employees GROUP BY position; 

#28
SELECT COUNT(batch_number) FROM alcohol_batches_to_storages GROUP BY storage_number HAVING COUNT(batch_number) != 1;

#29
SELECT SUM(bottle_count) AS count FROM alcohol_batches GROUP BY alcohol_brand ORDER BY count; 

#30
SELECT MIN(amount), cocktail_id FROM cocktails_to_alcohol_brands GROUP BY brand_id ORDER BY amount;

-- Запросы на: ORDER BY, ASC|DESC
#31
SELECT DISTINCT alcohol_class FROM alcohol_types ORDER BY alcohol_class DESC;

#32
SELECT name FROM manufacturers WHERE name LIKE '%r%' ORDER BY name;

#33
SELECT name FROM employees WHERE position IN (1, 2) ORDER BY name;

#34
SELECT ingredient, production_date FROM ingredient_batches ORDER BY production_date DESC;

#35
SELECT name, volume FROM glasses WHERE volume < 200 OR volume > 300 ORDER BY name;

-- Запросы на: Вложенные SELECTы
#36
SELECT name FROM employees WHERE position IN (SELECT id FROM positions WHERE id !=3);

#37
SELECT DISTINCT bottle_price FROM alcohol_batches 
	WHERE alcohol_brand IN (SELECT id FROM alcohol_brands 
		WHERE alcohol_class IN (SELECT id FROM alcohol_classes 
			WHERE name IN ("Водка", "Виски", "Текила"))); 
  
-- Запросы на: SELECT INTO
#38
CREATE TABLE info_left
	SELECT production_date, bottle_count, bottle_price, alcohol_brand FROM alcohol_batches 
		WHERE bottles_left > 2 AND bottles_left !=7;
DROP TABLE info_left;

#39
CREATE TABLE shelf_life 
	SELECT production_date, expiration_date FROM ingredient_batches
		WHERE count_remaining BETWEEN 6 AND 30;
SELECT * FROM shelf_life;
DROP TABLE shelf_life;
 
-- Запросы на: UNION (ALL), EXCEPT, INTERSECT
#40
SELECT name FROM alcohol_brands WHERE name LIKE '%a%'
UNION
SELECT name FROM cocktails WHERE name NOT IN('Отвертка', 'Ирландский кофе');

#41
SELECT name FROM alcohol_types
UNION
SELECT name FROM alcohol_classes
UNION 
SELECT name FROM ingredients
UNION
SELECT name FROM manufacturers;

#41
SELECT name FROM employees WHERE sex = 1 AND name NOT IN (SELECT name FROM employees WHERE position IN (2,3)) ORDER BY name;

#42
SELECT ingredient AS number_ingr FROM ingredient_batches 
WHERE production_date BETWEEN '2020-02-16' AND '2020-08-25'
AND ingredient IN (SELECT ingredient FROM ingredient_batches 
WHERE count_remaining != 12 OR count_remaining <= 30) ORDER BY number_ingr;

-- Запросы на: UPDATE (с WHERE)
#43
UPDATE employees SET date_of_birth = '1991-12-15' WHERE contract_number = 666666;

#44
UPDATE employees SET date_of_birth = '1991-12-14' WHERE contract_number = 666666;

#45
UPDATE cocktails SET price = '400' WHERE id = 9;

#46
UPDATE cocktails SET price = '350' WHERE id = 9;

#47
UPDATE alcohol_brands SET volume = '700' WHERE id = 7;

#48
UPDATE alcohol_brands SET volume = '1000' WHERE id = 7;

#49
UPDATE ingredient_batches SET production_date = '2020-12-19' WHERE number = 1;

#50
UPDATE ingredient_batches SET expiration_date = '2022-12-19' WHERE number = 1;

-- Запросы на: LIMIT
#51
(SELECT name FROM ingredients LIMIT 10) ORDER BY name;

#52
SELECT name FROM glasses LIMIT 2,6; 

#53
(SELECT name FROM alcohol_classes LIMIT 8,3) ORDER BY name;

#54
(SELECT wage FROM employees LIMIT 4) ORDER BY wage; 

#55
SELECT time FROM receipts LIMIT 6,4;
 
-- Запросы на: JOIN (20 шт.+): INNER, OUTTER (LEFT, RIGHT, FULL), CROSS, NATURAL
#56
SELECT name, count_remaining FROM (ingredients RIGHT JOIN ingredient_batches ON id = ingredient);

#57
SELECT * FROM (storages CROSS JOIN storage_types);

#58
SELECT * FROM alcohol_batches_to_storages JOIN ingredient_batches_to_storages 
ON alcohol_batches_to_storages.storage_number = ingredient_batches_to_storages.storage_number;

#59
SELECT name, volume, bottle_count FROM (alcohol_brands LEFT JOIN alcohol_batches ON alcohol_brand = id); 

#60
SELECT * FROM ((SELECT name, alcohol_class FROM alcohol_brands) AS t1
JOIN (SELECT name AS c_name, id AS c_id FROM alcohol_classes) AS t2 ON alcohol_class = c_id);

#61
SELECT cocktail_name, brand_name, amount, strength FROM
(SELECT cocktail_name, brand_id, amount FROM ((SELECT name AS cocktail_name, id AS c_id FROM cocktails) As t1  
JOIN cocktails_to_alcohol_brands ON c_id = cocktail_id)) AS t2
JOIN (SELECT name AS brand_name, strength, id AS b_id FROM alcohol_brands) AS t3 ON b_id = brand_id;

#62
SELECT * FROM ((SELECT ingredient_id AS id FROM cocktails_to_ingredients WHERE amount >10 AND amount <200) AS t1
NATURAL JOIN ingredients);

#63
SELECT amount, units, cocktail_id FROM ( cocktails_to_ingredients NATURAL JOIN cocktails_to_glasses);

#64
SELECT * FROM (SELECT name, id AS i_id FROM ingredients) AS t1 
LEFT JOIN (SELECT amount, units, ingredient_id FROM cocktails_to_ingredients) AS t2
ON i_id = ingredient_id ORDER BY name;

#65
SELECT * FROM (SELECT storage_number, batch_number FROM alcohol_batches_to_storages) AS t1
RIGHT JOIN (SELECT bottle_count, bottle_price, number FROM alcohol_batches WHERE bottle_price < 1500) AS t2 
ON batch_number = number;

#66
SELECT name, production_date, expiration_date, i_name FROM 
(SELECT name, production_date, expiration_date, ingredient FROM 
((SELECT name, id AS m_id FROM manufacturers) AS t1
JOIN (SELECT production_date, expiration_date, ingredient, manufacturer FROM 
ingredient_batches) AS t2 ON m_id = manufacturer)) AS t3 
JOIN (SELECT name AS i_name, id AS i_id FROM ingredients) AS t4 ON i_id = ingredient ORDER BY name;

#67
SELECT name, production_date, expiration_date, ingredient FROM (SELECT name, id AS m_id FROM manufacturers) AS t1
LEFT JOIN (SELECT production_date, expiration_date, ingredient, manufacturer 
FROM ingredient_batches) AS t2 ON m_id = manufacturer ORDER BY name;

#68
SELECT name, wage, adress FROM (SELECT name, wage, bar_id AS id FROM employees) AS t1 NATURAL JOIN bars;

#69
SELECT * FROM (receipts CROSS JOIN shifts) LIMIT 55;

#70
SELECT expiration_date, count_remaining, manufacturer, name FROM (SELECT expiration_date, count_remaining, manufacturer, ingredient FROM ingredient_batches) AS t1
RIGHT JOIN (SELECT name, id FROM ingredients) AS t2
ON ingredient = id;


#3
SELECT * FROM (((SELECT bar_id, number FROM storages) AS t1
JOIN (SELECT batch_number, storage_number FROM ingredient_batches_to_storages) AS t2 ON number = storage_number)
JOIN (SELECT manufacturer, ingredient, number AS b_number FROM ingredient_batches) AS t3 ON b_number = batch_number) 
NATURAL JOIN (SELECT name AS i_name FROM (SELECT name AS m_name FROM ingredients WHERE m_name = Bols Amsterdam));


#3
SELECT brand_name AS `Название`, class_name AS `Класс`, type_name AS `Тип` FROM
(SELECT brand_name, class_name, alcohol_type_id AS id FROM
(SELECT brand_name, alcohol_class AS id, alcohol_type_id FROM
(SELECT id, name AS brand_name, alcohol_class FROM alcohol_brands WHERE id IN -- Получаем бренды нужных партий
(SELECT alcohol_brand FROM alcohol_batches WHERE number IN -- Получаем id брендов нужных партий
(SELECT DISTINCT batch_number FROM alcohol_batches_to_storages WHERE batch_number IN -- Получаем номера партий, которые хранятся в заданных хранилищах и принадлежэат определённому бренду
(SELECT number FROM alcohol_batches WHERE alcohol_brand IN 	
(SELECT id FROM alcohol_brands WHERE manufacturer = (SELECT id FROM manufacturers WHERE name = 'Bols Amsterdam'))) -- Получаем список брендов конкретного производителя
AND storage_number IN(SELECT number FROM storages WHERE bar_id = (SELECT id FROM bars WHERE adress = 'Улица Советская 17'))))) AS t1 -- Получаем список хранилищ конкретного бара
NATURAL JOIN
(SELECT alcohol_brand_id AS id, alcohol_type_id FROM alcohol_brands_to_types) AS t2) AS t3	-- Получаем ссылку на тип
NATURAL JOIN
(SELECT id, name AS class_name FROM alcohol_classes) AS t4) AS t5 -- Добавляем название класса
NATURAL JOIN
(SELECT id, name AS type_name FROm alcohol_types) AS t6; -- Добавляем название типа


#2
SELECT name FROM
(SELECT cocktail_id FROM cocktails_to_alcohol_brands WHERE brand_id = (SELECT id FROM alcohol_brands WHERE name = 'Finlandia') -- Получаем id коктейля, где используется нужный бренд
AND cocktail_id IN -- Находим пересечение выборок
(SELECT cocktail_id FROM cocktails_to_ingredients WHERE ingredient_id = (SELECT id FROM ingredients WHERE name = 'Лед кубиками'))) AS t1 -- Получаем id коктейля, где используется нужный ингредиент
JOIN cocktails ON cocktail_id = id; -- Подставляем название коктейлей


#1
SELECT contract_number, name, p_name AS position, receipt_number, date, time FROM
(SELECT time, date, contract_number, receipt_number FROM 
(SELECT number AS receipt_number, time, date, shift_id FROM receipts WHERE date = '2020-10-12' AND time BETWEEN '19:00:00' AND '22:00:00') AS t1 -- ПОлучаем информацию о чеках в нужный временной отрезок
NATURAL JOIN (SELECT id AS shift_id, employee AS contract_number FROM shifts) AS t2) AS t3 -- Получаем контрактный номер сотрудника, который выписал чек
NATURAL JOIN (SELECT contract_number, name, position FROM employees WHERE bar_id = 1) AS t4 -- Отбираем сотрудников, которые работают в заданном баре
NATURAL JOIN (SELECT name AS p_name, id AS position FROM positions) AS t5; -- Добавляем название должности














