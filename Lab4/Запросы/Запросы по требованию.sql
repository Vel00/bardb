# 1 Получить информацию о алкоголе в наличии 
SELECT 
	name
FROM
	(
		alcohol_brands
	JOIN
		(
		SELECT DISTINCT 
			alcohol_brand
		FROM 
			alcohol_batches 
		WHERE
			bottles_left > 0 
		) AS brands_left
	ON
		alcohol_brands.id = brands_left.alcohol_brand
	)
ORDER BY
	name;

SELECT DISTINCT name FROM ( alcohol_brands JOIN alcohol_batches ON id = alcohol_brand) WHERE bottles_left>0 ORDER BY name;

# 2 Получить информацию об ингредиентах с истекающем сроком годности
SELECT 
	name
FROM
	(
		ingredients
	JOIN
		(
		SELECT DISTINCT 
			ingredient
		FROM 
			ingredient_batches 
		WHERE
			expiration_date <= CURDATE() 
		) AS expired
	ON
		ingredients.id = expired.ingredient
	)
ORDER BY
	name;

# 3 Получить информацию о ближайших сменах персонала 
SELECT
	id, date, time_from, time_to, name
FROM
	(
		(
		SELECT 
			id, employee, date, time_from, time_to
		FROM
			shifts
		WHERE 
			date 
		IN
			(CURDATE(), DATE_ADD( CURDATE(), INTERVAL 1 DAY ))
		) AS soon_shift
    JOIN
		employees
	ON 
		contract_number = employee
	)
ORDER BY
	name;
    
# 4 Получить информацию о заканчивающейся продукции
SELECT 
	name
FROM
	(
		ingredients
	JOIN
		(
		SELECT DISTINCT 
			ingredient
		FROM 
			ingredient_batches 
		WHERE
			count_remaining <= 10 
		) AS ingredients_left
	ON
		ingredients.id = ingredients_left.ingredient
	)
ORDER BY
	name;

# 5 Посчитать количество посетителей(чеков) за заданный период
SELECT COUNT(*) FROM receipts WHERE date BETWEEN '2020-10-12' AND '2020-10-16';
    
# 6 Минимальная цена бутылки
SELECT MIN( bottle_price ) FROM alcohol_batches;

# 7 Максимальная цена бутылки
SELECT MAX( bottle_price ) FROM alcohol_batches;

# 8 Минимальная цена коктейля
SELECT MIN( price ) FROM cocktails;

# 9 Максимальная цена коктейля
SELECT MAX( price ) FROM cocktails;

# 10 Узнать содержимое хранилища
	(
	SELECT name, count_remaining AS count FROM
			ingredients
		JOIN
			(
				ingredient_batches
			NATURAL JOIN
				(
				SELECT batch_number AS number FROM
					ingredient_batches_to_storages
				WHERE storage_number = 101
				) AS batches 
			)
		ON id = ingredient
	 )
UNION
	(
	SELECT name, bottles_left AS count FROM
			alcohol_brands
		JOIN
			(
				alcohol_batches
			NATURAL JOIN
				(
				SELECT batch_number AS number FROM
					alcohol_batches_to_storages
				WHERE storage_number = 101
				) AS batches 
			)
		ON id = alcohol_brand
	);

# 11 Возможная выручка с продажи определенной партии
SELECT bottle_price * bottle_count FROM alcohol_batches;

# 12 Посчитать сумму конкретного чека
SELECT SUM(total) FROM
(
	SELECT * FROM
    (
	SELECT bottle_price * amount AS total FROM 
		(SELECT number, bottle_price FROM alcohol_batches) AS a_price
	NATURAL JOIN 
		(SELECT amount, batch_number AS number FROM alcohol_batches_to_receipts WHERE receipt_number = 9) AS a_amount
	) AS a_total
UNION ALL
	(
	SELECT price * amount AS total FROM 
		(SELECT id, price FROM cocktails) AS c_price
	NATURAL JOIN 
		(SELECT amount, cocktail_id AS id FROM cocktails_to_receipts WHERE receipt_number = 9) AS c_amount
	) 
) AS t;

# 13 Количетво чеков на сотрудника за конкретную дату
SELECT COUNT(*) FROM 
	(SELECT date, employee, id FROM shifts) AS s
    JOIN
    (SELECT shift_id FROM receipts) AS r
    ON shift_id = id
WHERE employee = (SELECT contract_number FROM employees WHERE name = 'Максим')
AND date = '2020-10-15';

# 14 Максимальное количесмтво бутылок в партии
SELECT MAX(bottle_count) FROM alcohol_batches;

# 15 Имена сотрудников, занимающих определенную должность
SELECT name FROM employees WHERE position = (SELECT id FROM positions WHERE name = 'Бармен');
    
#16 Количество алкоголя в определённом коктейле
SELECT amount, cocktail_id FROM cocktails_to_alcohol_brands ORDER BY cocktail_id;

#17 Получить информацию о зарплате конкретного сотрудника
SELECT wage FROM employees  WHERE name = 'Алиса';

#18 Соотвествие коктейлей и бокалов
SELECT cocktail_name, glass_name FROM ((( SELECT name AS cocktail_name, id AS c_id FROM cocktails) AS t1
JOIN ( SELECT cocktail_id, glass_id FROM cocktails_to_glasses ) AS t2
ON cocktail_id = c_id)
NATURAL JOIN (SELECT name AS glass_name, id AS glass_id FROM glasses) AS t3);

#19 Средняя цена за коктейль
SELECT AVG(price) FROM cocktails;

#20 Средняя цена бутылки
SELECT AVG(bottle_price) FROM alcohol_batches; 






