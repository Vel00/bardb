#1
CREATE INDEX employee_name ON employees(name);

#2
CREATE INDEX position_name ON positions(name);

#3
CREATE INDEX brand_name ON alcohol_brands(name);
CREATE INDEX ingredient_name ON ingredients(name);

#4
CREATE INDEX receipt_date_time ON receipts(date, time);

#5
CREATE INDEX ingredient_production_date ON ingredient_batches(production_date);
ALTER TABLE ingredient_batches DROP INDEX ingredient_production_date;