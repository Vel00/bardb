RENAME TABLE bleklist TO blacklist;
RENAME TABLE mades TO maids;

ALTER TABLE check_in_records 
	ADD COLUMN pets BOOLEAN NOT NULL;
ALTER TABLE check_in_records 
	ADD COLUMN guest_count INT UNSIGNED NOT NULL;

ALTER TABLE eviction_records 
	ADD COLUMN comments text(1000) NOT NULL,
	ADD COLUMN  eviction_cause VARCHAR(255) NOT NULL;
   
ALTER TABLE rooms RENAME COLUMN stat TO state;

ALTER TABLE room_types RENAME COLUMN additional TO additional_places;

ALTER TABLE reservations ADD CONSTRAINT type_id_reservations
		FOREIGN KEY (type_id)
        REFERENCES room_types(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE;	
        
ALTER TABLE	rooms_to_types ADD CONSTRAINT type_id_rooms
		FOREIGN KEY (type_id)
        REFERENCES room_types(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE;


ALTER TABLE eviction_records
	DROP CONSTRAINT room_number_eviction;
ALTER TABLE eviction_records
    DROP COLUMN room_number;
    
ALTER TABLE blacklist
	DROP COLUMN blacklisting_date;
    
    
    
INSERT INTO room_types (name, places, additional_places) VALUES
('Econom-1', 1, 0),
('Econom-2', 2, 0);

INSERT INTO rooms (number, state, price_for_stay, rating) VALUES
(1, 'normal', 1500, 4),
(2, 'acceptable', 1500, 3),
(3, 'good', 2000, 5),
(4, 'normal', 2000, 4);

INSERT INTO rooms_to_types (type_id, room_number) VALUES
((SELECT id FROM room_types WHERE name = 'Econom-1'), 1),
((SELECT id FROM room_types WHERE name = 'Econom-2'), 2),
((SELECT id FROM room_types WHERE name = 'Econom-1'), 3),
((SELECT id FROM room_types WHERE name = 'Econom-2'), 4);

INSERT INTO room_types (name, places, additional_places) VALUES
('Comfort-1', 1, 1),
('Comfort-2', 2, 1);

INSERT INTO facilities (name) VALUES
('TV'), ('Air conditioning');

INSERT INTO rooms (number, state, price_for_stay, rating, facility_id) VALUES
(5, 'normal', 2500, 4, NULL),
(6, 'normal', 2500, 4, (SELECT id FROM facilities WHERE name = 'Air conditioning')),
(7, 'good', 4000, 5, (SELECT id FROM facilities WHERE name = 'TV')),
(8, 'normal', 3500, 4, NULL);


INSERT INTO rooms_to_types (type_id, room_number) VALUES
((SELECT id FROM room_types WHERE name = 'Comfort-1'), 5),
((SELECT id FROM room_types WHERE name = 'Comfort-1'), 6),
((SELECT id FROM room_types WHERE name = 'Comfort-2'), 7),
((SELECT id FROM room_types WHERE name = 'Comfort-2'), 8);

INSERT INTO maids (name) VALUES
('Alice'),
('Stefany');

INSERT INTO rooms_to_maids (maid_name, room_number) VALUES
('Alice', 1), ('Alice', 2), ('Alice', 5),('Alice', 6),
('Stefany', 3), ('Stefany', 4), ('Stefany', 7), ('Stefany', 8);

INSERT INTO guests (name, birth_date, passport_number, priveleges) VALUES
('Nokolas', '1988-07-19', '2098413834', FALSE),
('Mathew', '1979-10-03', '2079589298', TRUE);




