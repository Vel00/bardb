DROP TABLE guests_to_eviction_records;
DROP TABLE guests_to_check_in_records;
DROP TABLE eviction_records;
DROP TABLE check_in_records;
DROP TABLE administrators;
DROP TABLE rooms_to_types;
DROP TABLE rooms_to_maids;
DROP TABLE maids;
DROP TABLE rooms;
DROP TABLE facility_values;
DROP TABLE facilities;
DROP TABLE reservations;
DROP TABLE room_types;
DROP TABLE blacklist;
DROP TABLE guests;

DROP DATABASE hotel;