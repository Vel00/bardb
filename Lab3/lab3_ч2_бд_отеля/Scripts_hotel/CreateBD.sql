CREATE DATABASE IF NOT EXISTS hotel
CHARACTER SET = "utf8";
USE hotel;

CREATE TABLE IF NOT EXISTS guests
(
	id INT UNSIGNED NOT NULL auto_increment,
	name VARCHAR(255) NOT NULL,
    birth_date DATE NOT NULL,
    passport_number VARCHAR(10) NOT NULL,
    previous_stays text(1000) NULL,
    priveleges BOOLEAN NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS bleklist
(
	guest_id INT UNSIGNED NOT NULL,
    cause VARCHAR(255) NOT NULL,
    blacklisting_date DATE NOT NULL,
    PRIMARY KEY (guest_id),
    CONSTRAINT guest_id_blacklist
		FOREIGN KEY (guest_id)
        REFERENCES guests(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS room_types
(
	id INT UNSIGNED NOT NULL auto_increment,
    places INT UNSIGNED NOT NULL,
    additional INT UNSIGNED NOT NULL,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS reservations
(
	id INT UNSIGNED NOT NULL auto_increment,
    count INT UNSIGNED NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    guest_id INT UNSIGNED NOT NULL,
    type_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT guest_id_reservations
		FOREIGN KEY (guest_id)
        REFERENCES guests(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS facilities
(
	id INT UNSIGNED NOT NULL auto_increment,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS facility_values
(
	facility_id INT UNSIGNED NOT NULL auto_increment,
    value VARCHAR(50) NOT NULL,
    PRIMARY KEY (facility_id),
	CONSTRAINT facility_id_facility_values
		FOREIGN KEY (facility_id)
        REFERENCES facilities(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS rooms
(
	number INT UNSIGNED NOT NULL,
    stat VARCHAR(255) NOT NULL,
    price_for_stay INT UNSIGNED NOT NULL,
    rating INT UNSIGNED NOT NULL,
    facility_id INT UNSIGNED NULL,
    PRIMARY KEY (number),
	CONSTRAINT facility_id_rooms
		FOREIGN KEY (facility_id)
        REFERENCES facilities(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);	

CREATE TABLE IF NOT EXISTS mades
(
	name VARCHAR(255) NOT NULL,
    PRIMARY KEY (name)
);

CREATE TABLE IF NOT EXISTS rooms_to_maids
(
	id INT UNSIGNED NOT NULL auto_increment,
    room_number INT UNSIGNED NOT NULL,
    maid_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT room_number_maids
		FOREIGN KEY (room_number)
        REFERENCES rooms(number)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
	CONSTRAINT maid_name_rooms
		FOREIGN KEY (maid_name)
        REFERENCES mades(name)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS rooms_to_types
(
	id INT UNSIGNED NOT NULL auto_increment,
    type_id INT UNSIGNED NOT NULL,
    room_number INT UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT room_number_types
		FOREIGN KEY (room_number)
        REFERENCES rooms(number)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS administrators
(
	name VARCHAR(255) NOT NULL,
    PRIMARY KEY (name)
);

CREATE TABLE IF NOT EXISTS check_in_records
(
	id INT UNSIGNED NOT NULL auto_increment,
    check_in_date DATE NOT NULL,
    duratiON INT UNSIGNED NOT NULL,
    comments text(1000) NULL,
	method_of_payment VARCHAR(10) NOT NULL,
    administrator_name VARCHAR(255) NOT NULL,
    room_number	INT UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT room_number_check_in
		FOREIGN KEY (room_number)
        REFERENCES rooms(number)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
	CONSTRAINT administrator_name_check_in
		FOREIGN KEY (administrator_name)
        REFERENCES administrators(name)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS eviction_records
(
	id INT UNSIGNED NOT NULL auto_increment,
    additional_payment INT UNSIGNED NOT NULL,
    eviction_date DATE NOT NULL,
    administrator_name VARCHAR(255) NOT NULL,
    room_number	INT UNSIGNED NOT NULL,
    PRIMARY KEY (id),
	CONSTRAINT administrator_name_eviction
		FOREIGN KEY (administrator_name)
        REFERENCES administrators(name)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
	CONSTRAINT room_number_eviction
		FOREIGN KEY (room_number)
        REFERENCES rooms(number)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS guests_to_check_in_records
(
	id INT UNSIGNED NOT NULL auto_increment,
    guest_id INT UNSIGNED NOT NULL,
    record_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT guest_id_check_in_records
		FOREIGN KEY (guest_id)
        REFERENCES guests(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
	CONSTRAINT check_in_records_guests
		FOREIGN KEY (record_id)
        REFERENCES check_in_records(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS guests_to_eviction_records
(
	id INT UNSIGNED NOT NULL auto_increment,
    guest_id INT UNSIGNED NOT NULL,
    record_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT guest_id_eviction_records
		FOREIGN KEY (guest_id)
        REFERENCES guests(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
	CONSTRAINT eviction_records_guests
		FOREIGN KEY (record_id)
        REFERENCES eviction_records(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);



