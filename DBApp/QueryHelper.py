# организует хранение и вызов запросов
class QueryHelper:
    def __init__(self, db):
        self.DB = db
        self.query_list = [
            '''SELECT name, wage FROM employees  WHERE name = '{name}';''',
            '''SELECT cocktail_name, glass_name FROM ((( SELECT name AS cocktail_name, id AS c_id FROM cocktails) AS t1
               JOIN ( SELECT cocktail_id, glass_id FROM cocktails_to_glasses ) AS t2
               ON cocktail_id = c_id)
               NATURAL JOIN (SELECT name AS glass_name, id AS glass_id FROM glasses) AS t3);''',
            '''SELECT COUNT(*) AS c FROM (SELECT date, employee, id FROM shifts) AS s
               JOIN (SELECT shift_id FROM receipts) AS r ON shift_id = id
               WHERE employee = (SELECT contract_number FROM employees WHERE name = '{name}') AND date = '{date}';''',
            '''SELECT name FROM
               (SELECT cocktail_id FROM cocktails_to_alcohol_brands 
               WHERE brand_id = (SELECT id FROM alcohol_brands WHERE name = '{brand}') AND cocktail_id IN 
               (SELECT cocktail_id FROM cocktails_to_ingredients WHERE ingredient_id = 
               (SELECT id FROM ingredients WHERE name = '{ingredient}'))) AS t1 
               JOIN cocktails ON cocktail_id = id;''',
            '''SELECT alcohol_types.id AS type FROM alcohol_brands 
               JOIN alcohol_brands_to_types ON alcohol_brands.id = alcohol_brand_id
               JOIN alcohol_types ON alcohol_type_id = alcohol_types.id
               WHERE alcohol_brands.id = {id}'''
        ]

    # вызывает запрос под заданым индексом с переданными аргументами
    def execute(self, index, **kwargs):
        query = self.query_list[index].format(**kwargs)
        return self.DB.query(query)