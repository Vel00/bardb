import pymysql


# класс связи с базой данных
class DBController:
    def __init__(self):
        self.host = 'localhost'
        self.user = 'python'
        self.password = 'python'
        self.dbname = 'bar_db'
        self.connection = None

    def connect(self):
        self.connection = pymysql.connect(self.host, self.user, self.password, self.dbname,
                                          cursorclass=pymysql.cursors.DictCursor, autocommit=True)

    def disconnect(self):
        self.connection.close()

    # получение всех записей из заданной таблицы
    def get_from_table(self, table_name):
        self.connect()
        with self.connection.cursor() as cursor:
            cursor.execute('SELECT * FROM ' + table_name + ';')

            items = cursor.fetchall()

        self.disconnect()
        return items

    # получение всех записей из заданной таблицы с условием
    def get_from_table_where(self, table_name, where):
        self.connect()
        with self.connection.cursor() as cursor:
            cursor.execute('SELECT * FROM ' + table_name + ' WHERE ' + where + ';')

            items = cursor.fetchall()

        self.disconnect()
        return items

    # обновление заданной записи
    def update_table(self, table_name, column_name, new_value, primary_key, key_value):
        self.connect()
        with self.connection.cursor() as cursor:
            if isinstance(new_value, int):
                a = '%d'
            elif isinstance(new_value, float):
                a = '%f'
            else:
                a = '\'%s\''

            query = 'UPDATE {} SET {} = {} WHERE {} = %d;'.format(table_name, column_name, a, primary_key)
            query = query % (new_value, key_value)
            cursor.execute(query)

        self.disconnect()

    # добаление новой записи в таблицу
    def insert_into_table(self, table_name, new_row):
        self.connect()
        with self.connection.cursor() as cursor:
            value_list = list(new_row.values())
            value_list = ["'{}'".format(value) if type(value) == str else str(value) for value in value_list]
            query = 'INSERT INTO {}({}) VALUES ({});'.format(table_name, ', '.join(list(new_row.keys())), ', '.join(value_list))
            cursor.execute(query)

        self.disconnect()

    # удаление записи из таблицы
    def delete(self, table_name, key_name, key_value):
        self.connect()
        with self.connection.cursor() as cursor:
            query = 'DELETE FROM {} WHERE {}={}'.format(table_name, key_name, key_value)
            cursor.execute(query)

        self.disconnect()
            
    # вызов заданного запроса
    def query(self, query):
        self.connect()
        with self.connection.cursor() as cursor:
            cursor.execute(query)

        self.disconnect()
        return cursor.fetchall()


if __name__ == '__main__':
    dbcontroller = DBController()
    dbcontroller.delete('alcohol_classes', 'id', '14')
    dbcontroller.get_from_table('alcohol_classes')
