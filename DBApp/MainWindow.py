from main_window_ui import Ui_MainWindow
from DBController import DBController
from PyQt5.Qt import *
from QueryHelper import QueryHelper


# класс главного окна
class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        self.DB = DBController()    # экземпляр контроллера базы данных
        self.QH = QueryHelper(self.DB)

        # данные таблиц
        self.content_employees = []
        self.content_alcohol_brands = []
        self.content_alcohol_classes = []
        self.content_alcohol_types = []
        self.types = []
        self.content_positions = []
        self.content_bars = []
        self.content_manufacturers = []

        # флаги, что добавляется новая запись
        self.new_employee = False
        self.new_brand = False
        self.new_class = False
        self.new_type = False

        # соединение сигналов и слотов
        self.employee_table.currentCellChanged.connect(self.on_employee_selected)
        self.brand_table.currentCellChanged.connect(self.on_brand_selected)
        self.class_table.currentCellChanged.connect(self.on_class_selected)
        self.type_table.currentCellChanged.connect(self.on_type_selected)

        self.employee_revert.clicked.connect(self.on_employee_revert)
        self.brand_revert.clicked.connect(self.on_brand_revert)
        self.class_revert.clicked.connect(self.on_class_revert)
        self.type_revert.clicked.connect(self.on_type_revert)

        self.employee_apply.clicked.connect(self.on_employee_apply)
        self.brand_apply.clicked.connect(self.on_brand_apply)
        self.class_apply.clicked.connect(self.on_class_apply)
        self.type_apply.clicked.connect(self.on_type_apply)

        self.employee_add.clicked.connect(self.on_employee_add)
        self.brand_add.clicked.connect(self.on_brand_add)
        self.class_add.clicked.connect(self.on_class_add)
        self.type_add.clicked.connect(self.on_type_add)

        self.employee_delete.clicked.connect(self.on_employee_delete)
        self.brand_delete.clicked.connect(self.on_brand_delete)
        self.class_delete.clicked.connect(self.on_class_delete)
        self.type_delete.clicked.connect(self.on_type_delete)

        self.brand_alcohol_class.currentIndexChanged.connect(self.fill_type_list)
        self.remove_brand_type.clicked.connect(self.on_brand_type_remove)
        self.add_brand_type.clicked.connect(self.on_brand_type_add)

        self.query1.clicked.connect(self.on_query1)
        self.query2.clicked.connect(self.on_query2)
        self.query3.clicked.connect(self.on_query3)
        self.query4.clicked.connect(self.on_query4)

        self.fill_tables()

    # заполнение таблиц из базы данных
    def fill_tables(self):
        self.fill_employees()
        self.employee_table.setCurrentCell(0, 0)

        self.fill_alcohol_brands()
        self.brand_table.setCurrentCell(0, 0)

        self.fill_ingredients()

        self.fill_alcohol_classes()
        self.class_table.setCurrentCell(0, 0)

        id = -1 if len(self.content_alcohol_classes) == 0 else self.content_alcohol_classes[0]['id']

        self.fill_alcohol_types(id)
        self.type_table.setCurrentCell(0, 0)

    # заполнение из заданнной таблицы
    def fill_table(self, table_widget, content):
        if len(content) == 0:
            table_widget.setRowCount(0)
            return
        keys = list(content[0].keys())
        table_widget.setRowCount(len(content))
        table_widget.setColumnCount(len(keys))
        table_widget.setHorizontalHeaderLabels(keys)
        table_widget.verticalHeader().hide()
        for i in range(len(content)):
            for j in range(len(keys)):
                item = QTableWidgetItem(str(content[i][keys[j]]))
                table_widget.setItem(i, j, item)
        table_widget.resizeColumnsToContents()

    # заполнение таблицы сотрудницов
    def fill_employees(self):
        content = self.DB.get_from_table('employees')
        self.content_employees = content
        self.fill_table(self.employee_table, content)

        self.employee_names.clear()
        self.employee_names_q3.clear()
        for i in range(len(content)):
            self.employee_names.addItem(content[i]['name'])
            self.employee_names_q3.addItem(content[i]['name'])

        content = self.DB.get_from_table('positions')
        self.content_positions = content

        self.employee_position.clear()
        for i in range(len(content)):
            self.employee_position.addItem(content[i]['name'], content[i]['id'])

        content = self.DB.get_from_table('bars')
        self.content_bars = content

        self.employee_bar.clear()
        for i in range(len(content)):
            self.employee_bar.addItem(content[i]['adress'], content[i]['id'])

    # заполнение таблицы брендов
    def fill_alcohol_brands(self):
        content = self.DB.get_from_table('alcohol_brands')
        self.content_alcohol_brands = content
        self.fill_table(self.brand_table, content)

        self.alcohol_name.clear()
        for i in range(len(content)):
            self.alcohol_name.addItem(content[i]['name'])

        content = self.DB.get_from_table('manufacturers')
        self.content_manufacturers = content

        self.brand_manufacturer.clear()
        for i in range(len(content)):
            self.brand_manufacturer.addItem(content[i]['name'], content[i]['id'])

    # заполение списка ингредиентов
    def fill_ingredients(self):
        content = self.DB.get_from_table('ingredients')

        self.ingredient_name.clear()
        for i in range(len(content)):
            self.ingredient_name.addItem(content[i]['name'])

    # заполенение таблицы классов алкоголя
    def fill_alcohol_classes(self):
        content = self.DB.get_from_table('alcohol_classes')
        self.content_alcohol_classes = content
        self.fill_table(self.class_table, content)

        self.brand_alcohol_class.clear()
        for i in range(len(content)):
            self.brand_alcohol_class.addItem(content[i]['name'], content[i]['id'])

    # заполенение таблицы классов алкоголя
    def fill_alcohol_types(self, class_id):
        if class_id == -1:
            content = self.DB.get_from_table('alcohol_types')
        else:
            content = self.DB.get_from_table_where('alcohol_types', 'alcohol_class = ' + str(class_id))
        self.content_alcohol_types = content
        self.fill_table(self.type_table, content)

    # отображение выбранного сотрудника
    def on_employee_selected(self, row):
        if row == -1:
            return
        selected_employee = self.content_employees[row]
        self.employee_contract_number.setValue(selected_employee['contract_number'])
        self.employee_name.setText(selected_employee['name'])
        self.employee_date_of_birth.setDate(QDate.fromString(str(selected_employee['date_of_birth']), Qt.ISODate))
        self.employee_wage.setValue(selected_employee['wage'])

        if selected_employee['sex'] == 1:
            self.employee_sex_male.click()
        else:
            self.employee_sex_female.click()

        self.employee_position.setCurrentIndex(self.employee_position.findData(selected_employee['position']))
        self.employee_bar.setCurrentIndex(self.employee_bar.findData(selected_employee['bar_id']))

    # отображение выбранного бренда
    def on_brand_selected(self, row):
        if row == -1:
            return
        selected_brand = self.content_alcohol_brands[row]
        self.brand_name.setText(selected_brand['name'])
        self.brand_volume.setValue(selected_brand['volume'])
        self.brand_srength.setValue(selected_brand['strength'])
        self.brand_alcohol_class.setCurrentIndex(self.brand_alcohol_class.findData(selected_brand['alcohol_class']))
        self.brand_manufacturer.setCurrentIndex(self.brand_manufacturer.findData(selected_brand['manufacturer']))

        self.fill_type_list()

    def fill_type_list(self):
        data = self.brand_alcohol_class.currentData()
        if data is None:
            return

        self.types = self.DB.get_from_table_where('alcohol_types', 'alcohol_class = ' + str(data))
        if not self.types:
            self.type_list.clear()
            return

        types = {t['id']: t['name'] for t in self.types}
        selected_types = self.QH.execute(4, id=self.content_alcohol_brands[self.brand_table.currentRow()]['id'])

        self.type_list.clear()
        if data != self.content_alcohol_brands[self.brand_table.currentRow()]['alcohol_class']:
            return

        for t in selected_types:
            item = QListWidgetItem(types[t['type']])
            item.setData(Qt.UserRole, t['type'])
            self.type_list.addItem(item)

    # отображение выбранного класса
    def on_class_selected(self, row):
        if row == -1:
            return
        selected_class = self.content_alcohol_classes[row]
        self.class_name.setText(selected_class['name'])
        self.fill_alcohol_types(selected_class['id'])
        self.type_table.setCurrentCell(0, 0)

    # отображение выбранного типа
    def on_type_selected(self, row):
        if row == -1:
            return
        selected_type = self.content_alcohol_types[row]
        self.type_name.setText(selected_type['name'])

    # сброс изменений в данных сотрудника
    def on_employee_revert(self):
        self.on_employee_selected(self.employee_table.currentRow())

    # сброс изменений в данных алкоголя
    def on_brand_revert(self):
        self.on_brand_selected(self.brand_table.currentRow())

    # сброс изменений в данных класса алкоголя
    def on_class_revert(self):
        self.on_class_selected(self.class_table.currentRow())

    # сброс изменений в данных типа алкоголя
    def on_type_revert(self):
        self.on_type_selected(self.type_table.currentRow())

    # принятие изменений в данных сотрдуника
    def on_employee_apply(self):
        if self.employee_sex_male.isChecked():
            value = 1
        else:
            value = 0
        if not self.new_employee:
            pk_name = 'contract_number'
            pk_value = self.content_employees[self.employee_table.currentRow()][pk_name]

            self.DB.update_table('employees', 'name', self.employee_name.text(), pk_name, pk_value)
            self.DB.update_table('employees', 'date_of_birth', self.employee_date_of_birth.date().toPyDate(), pk_name, pk_value)
            self.DB.update_table('employees', 'wage', self.employee_wage.value(), pk_name, pk_value)

            self.DB.update_table('employees', 'sex', value, pk_name, pk_value)
            self.DB.update_table('employees', 'position', self.employee_position.currentData(), pk_name, pk_value)
            self.DB.update_table('employees', 'bar_id', self.employee_bar.currentData(), pk_name, pk_value)
            self.DB.update_table('employees', 'contract_number', self.employee_contract_number.value(), pk_name, pk_value)
            new_row = self.employee_table.currentRow()
        else:
            self.new_employee= False
            new_employee = {
                'contract_number':  self.employee_contract_number.value(),
                'name': self.employee_name.text(),
                'date_of_birth':  self.employee_date_of_birth.date().toPyDate(),
                'wage': self.employee_wage.value(),
                'sex': value,
                'position': self.employee_position.currentData(),
                'bar_id': self.employee_bar.currentData()
            }
            self.DB.insert_into_table('employees', new_employee)
            new_row = self.employee_table.rowCount()

        self.fill_tables()
        self.employee_table.setCurrentCell(new_row, 0)

    # принятие изменений в данных алкоголя
    def on_brand_apply(self):
        if not self.new_brand:
            pk_name = 'id'
            pk_value = self.content_alcohol_brands[self.brand_table.currentRow()][pk_name]

            self.DB.update_table('alcohol_brands', 'name', self.brand_name.text(), pk_name, pk_value)
            self.DB.update_table('alcohol_brands', 'volume', self.brand_volume.value(), pk_name, pk_value)
            self.DB.update_table('alcohol_brands', 'strength', self.brand_srength.value(), pk_name, pk_value)
            self.DB.update_table('alcohol_brands', 'alcohol_class', self.brand_alcohol_class.currentData(), pk_name, pk_value)
            self.DB.update_table('alcohol_brands', 'manufacturer', self.brand_manufacturer.currentData(), pk_name, pk_value)
            new_row = self.brand_table.currentRow()
        else:
            self.new_brand = False
            new_brand = {
                'name': self.brand_name.text(),
                'volume': self.brand_volume.value(),
                'strength': self.brand_srength.value(),
                'alcohol_class': self.brand_alcohol_class.currentData(),
                'manufacturer': self.brand_manufacturer.currentData()
            }
            self.DB.insert_into_table('alcohol_brands', new_brand)
            new_row = self.brand_table.rowCount()
            pk_value = self.DB.get_from_table('alcohol_brands')[-1]['id']

        self.update_brand_type(pk_value)

        self.fill_tables()
        self.brand_table.setCurrentCell(new_row, 0)

    # принятие изменений в данных класса алкоголя
    def on_class_apply(self):
        if not self.new_class:
            pk_name = 'id'
            pk_value = self.content_alcohol_classes[self.class_table.currentRow()][pk_name]

            self.DB.update_table('alcohol_classes', 'name', self.class_name.text(), pk_name, pk_value)
            new_row = self.class_table.currentRow()
        else:
            self.new_class = False
            new_class = {'name': self.class_name.text()}
            self.DB.insert_into_table('alcohol_classes', new_class)
            new_row = self.class_table.rowCount()

        self.fill_tables()
        self.class_table.setCurrentCell(new_row, 0)

    # принятие изменений в данных типа алкоголя
    def on_type_apply(self):
        if not self.new_type:
            pk_name = 'id'
            pk_value = self.content_alcohol_types[self.type_table.currentRow()][pk_name]

            self.DB.update_table('alcohol_types', 'name', self.type_name.text(), pk_name, pk_value)
            new_row = self.type_table.currentRow()
        else:
            self.new_type = False
            new_type = {
                'name': self.type_name.text(),
                'alcohol_class': self.content_alcohol_classes[self.class_table.currentRow()]['id']
            }
            self.DB.insert_into_table('alcohol_types', new_type)
            new_row = self.type_table.rowCount()

        self.fill_alcohol_types(self.content_alcohol_classes[self.class_table.currentRow()]['id'])
        self.type_table.setCurrentCell(new_row, 0)

    # добавление данных нового сотрудника
    def on_employee_add(self):
        self.new_employee = True
        self.employee_contract_number.setValue(100000)
        self.employee_name.setText('')
        self.employee_date_of_birth.setDate(QDate(1980, 1, 1))
        self.employee_wage.setValue(20000)
        self.employee_sex_male.click()
        self.employee_position.setValue(1)
        self.employee_bar_id.setValue(1)

    # добавление данных нового алкогольного бренда
    def on_brand_add(self):
        self.new_brand = True
        self.brand_name.setText('')
        self.brand_volume.setValue(1000)
        self.brand_srength.setValue(0)
        self.brand_alcohol_class.setCurrentIndex(0)
        self.brand_manufacturer.setCurrentIndex(0)

    # добавление данных нового алкогольного класса
    def on_class_add(self):
        self.new_class = True
        self.class_name.setText('')

    # добавление данных нового алкогольного типа
    def on_type_add(self):
        self.new_type = True
        self.type_name.setText('')

    # удаление данных выбранного сотрудника
    def on_employee_delete(self):
        try:
            current_row = self.employee_table.currentRow()
            contract_number = self.content_employees[current_row]['contract_number']
            self.DB.delete('employees', 'contract_number', contract_number)
            self.fill_employees()
            self.employee_table.setCurrentCell(current_row - 1, 0)
        except:
            QMessageBox.warning(self, 'Ошибка!', 'Невозможно удалить сотрудника, на который есть ссылка')

    # удаление данных выбранного алкогольного бренда
    def on_brand_delete(self):
        try:
            current_row = self.brand_table.currentRow()
            id = self.content_alcohol_brands[current_row]['id']
            self.DB.delete('alcohol_brands', 'id', id)
            self.fill_alcohol_brands()
            self.brand_table.setCurrentCell(current_row - 1, 0)
        except:
            QMessageBox.warning(self, 'Ошибка!', 'Невозможно удалить брэнд, на который есть ссылка')

    # удаленпие данных выбранного алкогольного класса
    def on_class_delete(self):
        try:
            current_row = self.class_table.currentRow()
            id = self.content_alcohol_classes[current_row]['id']
            self.DB.delete('alcohol_classes', 'id', id)
            self.fill_alcohol_classes()
            self.class_table.setCurrentCell(current_row - 1, 0)
        except:
            QMessageBox.warning(self, 'Ошибка!', 'Невозможно удалить класс, на который есть ссылка')

    # удаленпие данных выбранного алкогольного типа
    def on_type_delete(self):
        try:
            current_row = self.type_table.currentRow()
            id = self.content_alcohol_types[current_row]['id']
            self.DB.delete('alcohol_types', 'id', id)
            self.fill_alcohol_types(self.content_alcohol_classes[self.class_table.currentRow()]['id'])
            self.type_table.setCurrentCell(current_row - 1, 0)
        except:
            QMessageBox.warning(self, 'Ошибка!', 'Невозможно удалить тип, на который есть ссылка')

    # добавление определенного типа к выбранному брэнду
    def on_brand_type_add(self):
        selected_types = []
        for i in range(self.type_list.count()):
            selected_types.append(self.type_list.item(i).data(Qt.UserRole))
        available_types = [t for t in self.types if t['id'] not in selected_types]

        actions = []
        menu = QMenu(self)
        for t in available_types:
            action = QAction(t['name'])
            action.setData(t)
            menu.addAction(action)
            actions.append(action)

        action = menu.exec(QCursor.pos())
        if action is not None:
            t = action.data()
            item = QListWidgetItem(t['name'])
            item.setData(Qt.UserRole, t['id'])
            self.type_list.addItem(item)

    # удаление определенного типа у выбранного брэнда
    def on_brand_type_remove(self):
        self.type_list.takeItem(self.type_list.currentRow())

    # обновление типов у выбранного брэнда
    def update_brand_type(self, id):
        selected_types = []
        for i in range(self.type_list.count()):
            selected_types.append(self.type_list.item(i).data(Qt.UserRole))

        saved_types = [t['type'] for t in self.QH.execute(4, id=id)]

        types_to_add = [t for t in selected_types if t not in saved_types]
        types_to_delete = [t for t in saved_types if t not in selected_types]

        for type_id in types_to_add:
            self.DB.insert_into_table('alcohol_brands_to_types', {'alcohol_brand_id': id, 'alcohol_type_id': type_id})
        for type_id in types_to_delete:
            records = self.DB.get_from_table_where('alcohol_brands_to_types', f'alcohol_brand_id = {id} AND alcohol_type_id = {type_id}')
            self.DB.delete('alcohol_brands_to_types', 'id', records[0]['id'])

    # выполнение первого аналитического запроса
    def on_query1(self):
        result = self.QH.execute(0, name=self.employee_names.currentText())[0]
        self.query_result.setText(str('{name} - {wage} р.'.format(**result)))

    # выполнение второго аналитического запроса
    def on_query2(self):
        result = self.QH.execute(1)
        result_text = '\n'.join(['{cocktail_name} - {glass_name}'.format(**item) for item in result])
        self.query_result.setText(result_text)

    # выполнение третьего аналитического запроса
    def on_query3(self):
        result = self.QH.execute(2, name=self.employee_names_q3.currentText(), date=self.date_q3.date().toPyDate())
        self.query_result.setText('Количество чеков: ' + str(result[0]['c']))


    # выполнение четвертого аналитического запроса
    def on_query4(self):
        result = self.QH.execute(3, brand=self.alcohol_name.currentText(), ingredient=self.ingredient_name.currentText())
        if not result:
            self.query_result.setText('Коктейли не найдены')
            return
        result_text = '\n'.join(['{}'.format(item['name']) for item in result])
        self.query_result.setText(result_text)


if __name__ == '__main__':
    a = QApplication([])
    w = MainWindow()
    w.show()
    a.exec()
